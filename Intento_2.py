#!/usr/bin/env python3
#! -*- coding:utf-8 -*-

import time
import os
import random

def crear_matriz(ancho):

	matriz = []

	for i in range(ancho):
		matriz.append([])
		for j in range(ancho):
			numero = random.randint(0,1)

			if numero == 0:
				matriz[i].append('[X]')

			else:
				matriz[i].append([])

			print(matriz[i][j], end = "  ")
		print()

def menu():

	opcion = input("J / I: ")

#Meter todo esto dentro de un while True:

	if opcion.upper() == 'J':

		while True:
			try:
				ancho = int(input("Ingrese el ancho del cuadrado: "))
				crear_matriz(ancho)
				break

			except:
				print("\nDebe ingresar un número")

	elif opcion.upper() == 'I':

		print("La información del juego es: ")
		print("Cada célula viva con 2 o 3 células vecinas vivas sobrevive"
		"Cada célula con 4 o más células vecinas vivas muere por superpoblación"
		"Cada célula con 1 o ninguna célula vecina viva muere por soledad"
		"Cada célula muerta con 3 células vecinas vivas nace.")

		menu = input("Para volver al menú (S / N): ")

		if menu.upper() == 'S':
			menu()

		else:
			print("Hasta luego")

	else:
		print("Ha ingresado una opción no válida, por favor")


menu()
