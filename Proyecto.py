# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-

import time
import os
import random


def imprimir_matriz(matriz, ancho):

    # Largo de "-".
    largo = ((ancho*2) + 1)

    print("\n")
    print("-" * largo)
    for i in range(ancho):
        for j in range(ancho):
            print(matriz[i][j], end="")
        print('|')
    print("-" * largo)


def contador_vivas(matriz, ancho):

    vivas = 0

    # Recorre toda la matriz para contar células vivas.
    for i in range(ancho):
        for j in range(ancho):
            if(matriz[i][j] == '|X'):
                vivas += 1
            else:
                pass

    # Las muertas serán toda la matriz menos las vivas.
    muertas = ((ancho ** 2) - vivas)
    print("Células Vivas = ", vivas, "    Células Muertas = ", muertas)

    return vivas


def rellenar_matriz(matriz, ancho):

    # Rellena la matriz gracias a números generados aleatoriamente.
    for i in range(ancho):
        matriz.append([])
        for j in range(ancho):
            numero = random.randint(0, 1)

            if numero == 1:
                matriz[i].append('|X')

            elif numero == 0:
                matriz[i].append('| ')

            else:
                pass


def celulas_vecinas(matriz, ancho, x, y):

    contador = 0

    # Se crea una matriz de 3 x 3 para recorrer la matriz
    # original y así contar los vecinos que posea cada célula.
    # Problema: Recorre las 9 casillas existentes, incluyendo a la
    # que incluye a la célula.
    for i in range(x - 1, x + 2):
        if(i >= 0 and i < ancho):
            for j in range(y - 1, y + 2):
                # En solución al problema es que se salta la casilla central;
                # esto es para que no se cuente a sí misma.
                if(i == x and j == y):
                    continue

                if(j >= 0 and j < ancho):
                    if(matriz[i][j] == '|X'):
                        contador += 1

    return contador


def juego_conway(matriz, ancho):

    matriz_temporal = []

    # Creación de matriz_temporal.
    for i in range(ancho):
        matriz_temporal.append([])
        for j in range(ancho):
            matriz_temporal[i].append([])

    # Se realiza el cambio de la matriz_temporal con la original.
    for i in range(ancho):
        for j in range(ancho):
            matriz_temporal[i][j] = matriz[i][j]

    # Recorre toda la matriz validando las condiciones
    # que aparecen más abajo.
    for i in range(ancho):
        for j in range(ancho):

            # cantidad_cv será igual al contador (Cantidad de células vecinas)
            # que retorne la función celulas_vecinas.
            cantidad_cv = celulas_vecinas(matriz, ancho, i, j)

            if(matriz_temporal[i][j] == '|X' and (cantidad_cv == 2 or
                                                  cantidad_cv == 3)):
                matriz_temporal[i][j] = '|X'

            elif(matriz_temporal[i][j] == '|X' and cantidad_cv >= 4):
                matriz_temporal[i][j] = '| '

            elif(matriz_temporal[i][j] == '|X' and cantidad_cv < 2):
                matriz_temporal[i][j] = '| '

            elif(matriz_temporal[i][j] == '| ' and cantidad_cv == 3):
                matriz_temporal[i][j] = '|X'

    imprimir_matriz(matriz_temporal, ancho)

    # Se realiza el cambio de la matriz orginal con la matriz_temporal
    # para actualizar la matriz.
    for i in range(ancho):
        for j in range(ancho):
            matriz[i][j] = matriz_temporal[i][j]


def menu():

    matriz = []
    generacion = 0

    opcion = input("\nJugar (J) / Información (I): ")

    if opcion.upper() == 'J':

        # Se repite para que si o sí se ingrese un número.
        while True:
            try:
                os.system("clear")
                ancho = int(input("Ingrese el ancho del cuadrado (Solo números"
                                  " naturales): "))

                # Solo para que la matriz comience de 1 y no de 0.
                ancho += 1

                rellenar_matriz(matriz, ancho)
                os.system("clear")
                imprimir_matriz(matriz, ancho)
                os.system("clear")
                break

            except ValueError:
                print("\nDebe ingresar un número")

        while True:

            print("Juego de la vida de Conway")
            print("Generación ", generacion)

            juego_conway(matriz, ancho)
            contador_vivas(matriz, ancho)
            time.sleep(0.4)

            vivientes = contador_vivas(matriz, ancho)

            # Término del juego si las células vivas son 0.
            if(vivientes == 0):
                os.system('clear')
                print("\nLa vida se ha acabado en la generación", generacion)
                print("Press 'F' to pay respect\n")
                break

            os.system("clear")
            generacion += 1

    elif opcion.upper() == 'I':

        os.system("clear")
        print("\nLa información del juego es: ")
        print("\n -> Cada célula viva con 2 o 3 células vecinas vivas "
              "sobrevive."
              "\n -> Cada célula con 4 o más células vecinas vivas muere por "
              "superpoblación."
              "\n -> Cada célula con 1 o ninguna célula vecina viva muere por "
              "soledad."
              "\n -> Cada célula muerta con 3 células vecinas vivas nace.")

        volver = input("\nPara volver al menú (S / N): ")

        # Regresar al menú principal.
        if volver.upper() == 'S':
            os.system("clear")
            menu()

        else:
            os.system("clear")
            print("\nHasta luego\n")

    else:
        menu()


menu()
