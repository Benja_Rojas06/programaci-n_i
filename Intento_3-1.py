#!/usr/bin/env python3
#! -*- coding:utf-8 -*-

import time
import os
import random


def crear_matriz(matriz, ancho):

	largo = ((ancho * 2) + 1)
	vivas = 0

	print("\n")
	print("-" * largo)
	for i in range(ancho):
		matriz.append([])
		for j in range(ancho):
			numero = random.randint(0,1)

			if numero == 1:
				matriz[i].append('|X')
				vivas += 1

			elif numero == 0:
				matriz[i].append('| ')

			else:
				pass

			print(matriz[i][j], end = "")
		print('|')
	print("-" * largo)

	muertas = ((ancho ** 2) - vivas)

	print("Vivas = ", vivas, "\t  Muertas = ", muertas)

	return vivas


def celulas_vecinas(matriz, ancho, x, y):

	contador = 0

	for i in range(x - 1, x + 2):
		for j in range (y - 1, y + 2):
			if((i >= 0 and i < ancho) and (j >= 0 and j < ancho)):
				if (matriz[i][j] == '|X'):
					contador += 1

				else:
					contador = contador

			else:
				pass

	return contador


def juego_conway(matriz, ancho):

	matriz_temporal = []

	for i in range(ancho):
		matriz_temporal.append([])
		for j in range(ancho):
			matriz_temporal[i].append([])

	for i in range(ancho):
		for j in range(ancho):
			matriz_temporal[i][j] = matriz[i][j]

	for i in range(ancho):
		for j in range(ancho):

			cantidad_cv = celulas_vecinas(matriz, ancho, i, j)

			if(matriz[i][j] == '|X' and (cantidad_cv == 2 or cantidad_cv == 3)):
				matriz_temporal[i][j] = '|X'

			elif(matriz[i][j] == '|X' and cantidad_cv >= 4):
				matriz_temporal[i][j] = '| '

			elif(matriz[i][j] == '|X' and cantidad_cv <= 1):
				matriz_temporal[i][j] = '| '

			elif(matriz[i][j] == '| ' and cantidad_cv == 3):
				matriz_temporal[i][j] = '|X'

	for i in range(ancho):
		for j in range(ancho):
			matriz_temporal[i][j] = matriz[i][j]


def menu():

	matriz = []
	generacion = 0

	opcion = input("\nJugar (J) / Información (I): ")

	if opcion.upper() == 'J':

		while True:
			try:
				os.system("clear")
				ancho = int(input("Ingrese el ancho del cuadrado (Solo números"
				" naturales): "))

				while True:

					print("Juego de la vida de Conway")
					print("Generación ", generacion)

					crear_matriz(matriz, ancho)
					juego_conway(matriz, ancho)
					time.sleep(0.5)

					vivientes = crear_matriz(matriz, ancho)

					if(vivientes == 0):
						os.system('clear')
						print("F")
						break

					os.system("clear")
					generacion += 1

				break

			except:
				print("\nDebe ingresar un número")

	elif opcion.upper() == 'I':

		os.system("clear")
		print("\nLa información del juego es: ")
		print("\n -> Cada célula viva con 2 o 3 células vecinas vivas "
		"sobrevive."
		"\n -> Cada célula con 4 o más células vecinas vivas muere por "
		"superpoblación."
		"\n -> Cada célula con 1 o ninguna célula vecina viva muere por "
		"soledad."
		"\n -> Cada célula muerta con 3 células vecinas vivas nace.")

		volver = input("\nPara volver al menú (S / N): ")

		if volver.upper() == 'S':
			menu()

		else:
			print("\nHasta luego")

	else:
		menu()

menu()
